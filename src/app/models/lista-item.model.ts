
export class ListaItem {
    descripcion: string;
    compleatado: boolean;
    hora: Date;

    constructor (descripcion: string, horaItem: string){
        this.descripcion = descripcion;
        this.compleatado = false;
        this.hora = new Date();

    }
}