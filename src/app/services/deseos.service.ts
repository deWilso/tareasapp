import { Injectable } from '@angular/core';
import { Lista } from '../models/lista.model';

@Injectable({
  providedIn: 'root'
})
export class DeseosService {
  listas: Lista [] =[];

  constructor() {
    this.cargarStorage();
       }

   crearLista( titulo: string ){
     const nuevaLista = new Lista(titulo);
     this.listas.push(nuevaLista);
     this.guardarStorage();
     return nuevaLista.id;
   }
   guardarStorage(){
    localStorage.setItem('data', JSON.stringify(this.listas));
   }

   cargarStorage(){
    this.listas = (JSON.parse(localStorage.getItem('data'))) ? JSON.parse(localStorage.getItem('data')) : [];

   }

   obtenerLista(id: string | number) {
     id = Number(id);
     return this.listas.find(lista => lista.id ===id);
   }

  
}
