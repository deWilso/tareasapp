import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Lista } from 'src/app/models/lista.model';
import { AlertController, IonList } from '@ionic/angular';
import { DeseosService } from '../../services/deseos.service';
import { literalArr } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-listas',
  templateUrl: './listas.component.html',
  styleUrls: ['./listas.component.scss'],
})
export class ListasComponent implements OnInit {
  listas : Lista [] = [];
  @Input() terminados = true;
  @ViewChild('lista') lista: IonList;
  constructor(
    private router: Router,
    private AlertController: AlertController, 
    public DeseosService: DeseosService
  ) { 
    this.listas = DeseosService.listas;

  }

  ngOnInit() {
    
  }
  
  verLista(listaId: number){
    

    if (this.terminados === true){
      this.router.navigateByUrl(`/tabs/tab2/agregar/${listaId}`);

    }else{
      this.router.navigateByUrl(`/tabs/tab1/agregar/${listaId}`);

    }
  }

  async eliminarLista(lista: Lista, indexId?: number){
  
    const alert = await this.AlertController.create({
      header: 'Eliminar Lista',
      subHeader: `Desea realmente elimminar la lista: ${lista.titulo}?`,      
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Eliminar',
          cssClass: 'primary',
          handler: ()=>{
            this.listas.splice(indexId, 1);
            this.DeseosService.guardarStorage();
            this.lista.closeSlidingItems();

            }
        }
      ]
    });
    alert.present();
  }

  async editarNombre(lista: Lista){
    const alert = await this.AlertController.create({
      header: 'Modificar Lista',     
      inputs: [
        {
          name: 'nombre',
          type: 'text',
          value: lista.titulo
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          
        },
        {
          text: 'Modificar',
          cssClass: 'primary',
          handler: (data)=>{
            if( data.nombre.length === 0){
              return;
            }
            lista.titulo = data.nombre;
            this.DeseosService.guardarStorage();
            this.lista.closeSlidingItems();
            }
        }
      ]
    });
    alert.present();
  }

}
