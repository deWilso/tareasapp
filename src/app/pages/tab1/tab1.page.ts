import { Component } from '@angular/core';
import { DeseosService } from '../../services/deseos.service';
import { Lista } from '../../models/lista.model';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  listas: Lista  []=[];
  constructor(private deseosService: DeseosService,
     private router: Router,
     private AlertController: AlertController) {
    this.listas = deseosService.listas;
    
  }

  
  async agregarLista(){
    const alert = await this.AlertController.create({
      header: 'Nueva tarea ',
      inputs: [
        {
          name: 'titulo',
          type: 'text',
          placeholder: 'Escriba el nombre de la tarea'
          
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: ()=>{
            console.log('cancelado');
            }
        },
        {
          text: 'OK',
          cssClass: 'primary',
          handler: (data)=>{
            if (data.titulo.length === 0){
              return;
            }
            const listaId = this.deseosService.crearLista(data.titulo);
            this.router.navigateByUrl(`/tabs/tab1/agregar/${listaId}`);

            }
        }
      ]
    });
    alert.present();

  }

 

}

