import { Component, OnInit } from '@angular/core';
import { DeseosService } from '../../services/deseos.service';
import { Lista } from '../../models/lista.model';
import { ActivatedRoute } from '@angular/router';
import { ListaItem } from '../../models/lista-item.model';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {
  lista: Lista; 
  nombreItem = '';
  horaItem = '';
  constructor(
    private DeseosService: DeseosService,
    private route: ActivatedRoute
  ) {
    //en vez de hacer un observable para leer la url usamos este codigo pasandole el argumetno que deseamos leer de la ruta
    const listaId = this.route.snapshot.paramMap.get('listaId');
    this.lista = this.DeseosService.obtenerLista(listaId);
   
   }

  ngOnInit() {
  
  }
  guardarItem(){
    if (this.nombreItem.length === 0 || this.horaItem ===''){
      return;
    }
    const nuevoItem = new ListaItem( this.nombreItem, this.horaItem );
    this.lista.items.push( nuevoItem );
    this.nombreItem = '';
    this.DeseosService.guardarStorage();
   }

   itemCompletado(item: ListaItem){
     const itemsPenientes = this.lista.items.filter(item=>!item.compleatado).length;
     if (itemsPenientes === 0){
        this.lista.terminada = true;
        this.lista.terminada_en = new Date;
     } else {
      this.lista.terminada = false;
      this.lista.terminada_en = null;
     }
     this.DeseosService.guardarStorage();
   }

   eliminarItem(index: number){
     this.lista.items.splice(index, 1);
     this.DeseosService.guardarStorage();
     }

}
